# Advanced Data Structures in Java

    Highlights

- Introduction to the Course
- Introduction to Graphs
- Class design and simple graph search
- Finding shortest paths in weighted graphs
- Route planning and NP-hard graph problems
- End of Course Project Extension


### Assignments

      University of California, San Diego

`Introduction to Graphs`

- 2. Implement the degreeSequence method in the Graph class
Part 2: Implement the getDistance2 method in both GraphAdjList and GraphAdjMatrix implementations
- Adj
- Matrix

- Class design and simple graph search
public List<GeographicPoint> bfs(GeographicPoint start, GeographicPoint goal, Consumer<GeographicPoint> nodeSearched)



- Finding shortest paths in weighted graphs
- BFS
- Dijkstra
- A*

Route planning and NP-hard graph problems

`Completed Dec 2016`
